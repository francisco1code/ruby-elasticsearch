Source: ruby-elasticsearch
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Tim Potter <tpot@hp.com>
Build-Depends: debhelper-compat (= 12),
               gem2deb,
               ruby-ansi,
               ruby-curb,
               ruby-escape-utils,
               ruby-faraday (>= 1.0~),
               ruby-hashie,
               ruby-jbuilder,
               ruby-jsonify,
               ruby-mocha,
               ruby-multi-json,
               ruby-shoulda-context,
               ruby-test-unit,
               ruby-minitest-reporters
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-elasticsearch.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-elasticsearch
Homepage: http://github.com/elasticsearch/elasticsearch-ruby
XS-Ruby-Versions: all

Package: ruby-elasticsearch
X-DhRuby-Root: elasticsearch
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-elasticsearch-api (= ${source:Version}),
         ruby-elasticsearch-transport (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Ruby client for connecting to an Elasticsearch cluster
 Elasticsearch is a distributed RESTful search engine built for the cloud.
 .
 This package implements a client for connecting to an Elasticsearch
 cluster via the Elasticsearch REST interface.

Package: ruby-elasticsearch-api
X-DhRuby-Root: elasticsearch-api
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-multi-json,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Ruby implementation of the Elasticsearch REST API
 Elasticsearch is a distributed RESTful search engine built for the cloud.
 .
 This package provides a Ruby implementation of the Elasticsearch
 REST API. It is normally used as a support module for the
 ruby-elasticsearch package.

Package: ruby-elasticsearch-transport
X-DhRuby-Root: elasticsearch-transport
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-faraday (>= 1.0~),
         ruby-multi-json,
         ${misc:Depends},
         ${shlibs:Depends}
Description: low-level Ruby client for connecting to Elasticsearch
 Elasticsearch is a distributed RESTful search engine built for the cloud.
 .
 This package provides a low-level Ruby client for connecting to
 an Elasticsearch cluster. It is normally used as a support module for
 the ruby-elasticsearch package.

#Package: ruby-elasticsearch-extensions
#X-DhRuby-Root: elasticsearch-extensions
#Architecture: all
#XB-Ruby-Versions: ${ruby:Versions}
#Depends: ruby | ruby-interpreter,
#         ruby-elasticsearch,
#         ruby-ansi,
#         ${misc:Depends},
#         ${shlibs:Depends}
#Description: Ruby implementation with a set of extensions to the base library
# Elasticsearch is a distributed RESTful search engine built for the cloud.
# .
# This package provides a Ruby implementation with a set of extensions to the
# base library. It is normally used as a support module for the
# ruby-elasticsearch package.
#
#Package: ruby-elasticsearch-watcher
#X-DhRuby-Root: elasticsearch-watcher
#Architecture: all
#XB-Ruby-Versions: ${ruby:Versions}
#Depends: ruby | ruby-interpreter,
#         ruby-elasticsearch-api,
#         ${misc:Depends},
#         ${shlibs:Depends}
#Description: Ruby implementation of the Elasticsearch Watcher Plugin
# Elasticsearch is a distributed RESTful search engine built for the cloud.
# .
# This package provides a Ruby implementation of Ruby API for the
# Elasticsearch Watcher Plugin.It is normally used as a support module for
# the ruby-elasticsearch package.
#
#Package: ruby-elasticsearch-dsl
#X-DhRuby-Root: elasticsearch-dsl
#Architecture: all
#XB-Ruby-Versions: ${ruby:Versions}
#Depends: ruby | ruby-interpreter,
#         ${misc:Depends},
#         ${shlibs:Depends}
#Description: Ruby implementation to provides API of Elasticsearch Query DSL
# Elasticsearch is a distributed RESTful search engine built for the cloud.
# .
# This package provides a Ruby API for Elasticsearch Query DSL. It is
# normally used as a support module for the ruby-elasticsearch package.
