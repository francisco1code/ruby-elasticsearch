ruby-elasticsearch (6.8.2-2) unstable; urgency=medium

  * Team upload
  * Reupload to unstable

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

 -- Pirate Praveen <praveen@debian.org>  Tue, 01 Dec 2020 16:54:22 +0530

ruby-elasticsearch (6.8.2-1) experimental; urgency=medium

  * Team upload
  * Tighten dependency on ruby-faraday
  * New upstream version 6.8.2
  * Bump minimum version of ruby-faraday to 1.0
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Tue, 23 Jun 2020 23:50:35 +0530

ruby-elasticsearch (6.8.1-2) unstable; urgency=medium

  * Team upload
  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Mon, 30 Mar 2020 21:32:21 +0530

ruby-elasticsearch (6.8.1-1) experimental; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Pirate Praveen ]
  * New upstream version 6.8.1
  * Bump Standards-Version to 4.5.0 (no changes needed)
  * Refresh patches and delete disable-patron-test.patch
  * Disable elasticsearch-dsl tests (module not shipped and incompatible)

 -- Pirate Praveen <praveen@debian.org>  Thu, 27 Feb 2020 13:14:13 +0530

ruby-elasticsearch (5.0.5-2) unstable; urgency=medium

  * Team upload.
  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Thu, 09 Jan 2020 14:48:22 +0530

ruby-elasticsearch (5.0.5-1) experimental; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Macartur Sousa ]
  * Imported Upstream version 1.0.12
  * Imported Upstream version 2.0.0
  * Imported Upstream version 5.0.1

  [ Pirate Praveen ]
  * New upstream version 5.0.5
  * Refresh patches
  * Fix typo in patch description

 -- Pirate Praveen <praveen@debian.org>  Thu, 05 Dec 2019 19:02:55 +0530

ruby-elasticsearch (5.0.1-1) unstable; urgency=medium

  [ Macartur Carvalho ]
  * Team upload.
  * Adding docs to libs: extensions,dsl and watcher
  * Fixed warning without description in patch
  * Fixed arguments hash with retry_on_status
  * Published libs: extensions, dsl and watcher
  * Imported upstream version 2.0.0
  * Imported upstream version 5.0.1
  * Updated compact version from 9 to 10
  * Adding patch to fix bug when :retry_on_status is Nil::Class
  * Updated copyright

 -- Macártur Carvalho <macartur.sc@gmail.com>  Mon, 01 Aug 2016 16:50:15 -0300

ruby-elasticsearch (1.0.12-2) unstable; urgency=medium

  * Team upload
  * Rebuild for missing gemspec in .deb
  * Bump Standards-Version to 4.4.1 (no changes needed)
  * Use salsa.debian.org in Vcs-* fields
  * Drop compat file, rely on debhelper-compat and bump compat level to 12

 -- Sruthi Chandran <srud@debian.org>  Thu, 28 Nov 2019 20:48:40 +0530

ruby-elasticsearch (1.0.12-1) unstable; urgency=medium

  * Update to upstream version 1.0.12

 -- Tim Potter <tpot@hp.com>  Wed, 12 Aug 2015 11:28:29 +1000

ruby-elasticsearch (1.0.5-2) unstable; urgency=medium

  * Remove build/test dependency on ruby-patron.  The ruby-patron
    package is not currently in testing and is only one of a handful
    of transport-layer adapters and so does not affect any functionality.
    (Closes: #768613)

 -- Tim Potter <tpot@hp.com>  Tue, 11 Nov 2014 10:21:11 +1100

ruby-elasticsearch (1.0.5-1) unstable; urgency=low

  * Initial release (Closes: #760806)

 -- Tim Potter <tpot@hp.com>  Tue, 14 Oct 2014 15:56:12 +1100
